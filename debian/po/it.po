# Traduzione italiana di tasksel
# Copyright (C) 2004 Free Software Foundation, Inc.
# Stefano Canepa <sc@linux.it>, 2004,2005,2006
# Milo Casagrande <milo@ubuntu.com>, 2009.
# Ceppo <ceppo@oziosi.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: tasksel\n"
"Report-Msgid-Bugs-To: tasksel@packages.debian.org\n"
"POT-Creation-Date: 2018-05-23 01:37+0200\n"
"PO-Revision-Date: 2024-06-09 00:00+0000\n"
"Last-Translator: Ceppo <ceppo@oziosi.org>\n"
"Language-Team: Italian <debian-l10n-italian@lists.debian.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: multiselect
#. Description
#. Type: multiselect
#. Description
#: ../templates:1001 ../templates:2001
msgid "Choose software to install:"
msgstr "Scegliere il software da installare:"

#. Type: multiselect
#. Description
#: ../templates:1001
msgid ""
"At the moment, only the core of the system is installed. To tune the system "
"to your needs, you can choose to install one or more of the following "
"predefined collections of software."
msgstr ""
"Al momento, solo la parte principale del sistema è installata. Per adattare "
"l'installazione alle proprie esigenze, è possibile installare una o più "
"delle seguenti raccolte predefinite di software."

#. Type: multiselect
#. Description
#: ../templates:2001
msgid ""
"You can choose to install one or more of the following predefined "
"collections of software."
msgstr ""
"È possibile installare una o più delle seguenti raccolte predefinite di "
"software."

#. Type: multiselect
#. Description
#: ../templates:3001
msgid "This can be preseeded to override the default desktop."
msgstr "Questo può essere impostato per sostituire il desktop predefinito."

#. Type: title
#. Description
#: ../templates:4001
msgid "Software selection"
msgstr "Selezione del software"
